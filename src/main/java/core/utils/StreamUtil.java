package core.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class StreamUtil 
{
	public StreamUtil()
	{
		//Static method only
	}
	
	public static String getInputStream(Process proc) throws Exception
	{
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		
		return readAll(stdInput);
	}
	
	public static String getErrorStream(Process proc) throws Exception
	{
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
		
		return readAll(stdInput);
	}
	
	public static String getFileReader(String path) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(path));
		
		return readAll( br );
	}
	
	private static String readAll(BufferedReader br) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();

		while (line != null) 
		{
			sb.append(line);
			sb.append(System.lineSeparator());
			line = br.readLine();
		}
		
		String everything = sb.toString();
		
		return everything;	
	}
}
