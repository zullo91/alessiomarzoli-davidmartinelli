package core.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import core.enums.Functionalities;

public class FileUtils 
{

	public FileUtils() 
	{
		// static method only
	}


	public static String ReadFile(String path, Functionalities code ) throws Exception 
	{		
		String everything = StreamUtil.getFileReader(path);	
		
		//Get ref Head.
		if ( code == Functionalities.TEST0 )
		{
			//Remove refs: keyword  from HEAD file path .
			everything = everything.replace("ref: ", "");
			everything = everything.trim();
			return everything;
		}
		//Get hash code of Master.
		else if ( code == Functionalities.TEST1 )
		{
			//Clean the hash.
			everything = everything.trim();
			return everything;
		}	
		
		return null;
	}
	
	
}
