package core;

import java.io.File;

import core.enums.Functionalities;
import core.utils.FileUtils;

public class GitRepository 
{
	
	private File repoPath;

	public GitRepository(File repoPath) 
	{
		this.repoPath = repoPath;
	}

	public String getHeadRef() 
	{
		try
		{
			return FileUtils.ReadFile(repoPath.getPath() + "/HEAD", Functionalities.TEST0);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}

	public String getRefHash(String subPath) 
	{
		try
		{
			//System.out.println(FileUtils.ReadFile(repoPath.getPath() + "/" + subPath , 1));
			return FileUtils.ReadFile(repoPath.getPath() + "/" + subPath ,  Functionalities.TEST1);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	

}
