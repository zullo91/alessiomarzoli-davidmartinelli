package core;

import java.io.File;

import core.utils.StreamUtil;

public class GitBlobObject 
{
	private File repoPath;
	private String hash;
	
	public GitBlobObject(File repoPath, String hash)
	{
		this.repoPath = repoPath;
		this.hash = hash;
	}

	public String getType() throws Exception
	{
		Runtime rt = Runtime.getRuntime();
		
		Process pr = rt.exec("git cat-file -t 4452771b4a695592a82313e3253f5e073e6ead8c", null, repoPath);
		
		return StreamUtil.getInputStream(pr).trim();
	}

	public String getContent() throws Exception
	{
		Runtime rt = Runtime.getRuntime();
		
		Process pr = rt.exec("git cat-file -p 4452771b4a695592a82313e3253f5e073e6ead8c", null, repoPath);
		
		return StreamUtil.getInputStream(pr);
	}
	
	
}
