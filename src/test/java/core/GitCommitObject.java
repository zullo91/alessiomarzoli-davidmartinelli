package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import core.utils.StreamUtil;

public class GitCommitObject 
{
	
	private File repoPath;
	private String masterCommitHash;
	private GitRepository repository;
	private String masterTreeHash;
	
	public GitCommitObject(File repoPath, String masterCommitHash)
	{
		this.repoPath = repoPath;
		this.masterCommitHash = masterCommitHash;
		this.repository = new GitRepository(repoPath);
	}

	public String getHash() 
	{
		return repository.getRefHash(repository.getHeadRef());
	}

	public String getTreeHash() throws Exception
	{
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec("git cat-file -p master", null, repoPath);
		
		String s = StreamUtil.getInputStream(pr);

		s = findLineInString(s,"tree");
		s = removeStringFromLine(s, "tree").trim();
		
		return s;
	}

	public String getParentHash() throws Exception
	{
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec("git cat-file -p master", null, repoPath);
		
		String s = StreamUtil.getInputStream(pr);

		s = findLineInString(s,"parent");
		s = removeStringFromLine(s, "parent").trim();
				
		return s;
	}

	public String getAuthor() throws Exception
	{
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec("git cat-file -p master", null, repoPath);
		
		String s = StreamUtil.getInputStream(pr);

		s = findLineInString(s,"author");
		//s = removeStringFromLine(s, "tree").trim();
		
		s = getWordsFromSplittedString(s, 1, 2, 3);
		//Clear the endline hidden character. (\n)
		//.We don't need .trim method because of the porpouse of the string.
		s = s.substring(0, s.length()-1);
		System.out.println(s);
		return s;
	}
	
	private String findLineInString(String s,String word) 
	{	
	    String[] lines = s.split("\n");
	    
	    for (int i = 0; i < lines.length; i++) 
	    {
	    	//If find words get the line.
	    	if (lines[i].contains(word))
	    	{
	    		return lines[i];
	    	}
		}

		return null;
	}
	
	private String removeStringFromLine(String line, String word)
	{		
		return line.replace(word,"");
	}
	
	private String getWordsFromSplittedString(String s, Integer... index)
	{
		String[] words = s.split(" "); 
		
		StringBuilder sb = new StringBuilder();
		
		for(int i=0; i<words.length; i++)
		{
			for(int j=0; j<index.length; j++)
			{
				if(i == index[j])
					sb.append(words[i] + " ");
			}
		}
		
		return sb.toString();	
	}
}
